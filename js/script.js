// Obtener elementos del DOM
const editor = document.getElementById('editor');
const encode = document.getElementById('encode');
const decodeBtn = document.getElementById('decode');
const shareBtn = document.getElementById('share');
const copyMainBtn = document.getElementById('copy');
const deleteBtn = document.getElementById('delete');
const popupCopyBtn = document.getElementById('popup-copy');
const popupCloseBtn = document.getElementById('popup-close');

encode.addEventListener("click", encode_f);
decodeBtn.addEventListener("click", decode_f);
shareBtn.addEventListener("click", share_f);
copyMainBtn.addEventListener("click", copyMain_f);
deleteBtn.addEventListener("click", delete_f);
popupCopyBtn.addEventListener("click", copyPopup_f);
popupCloseBtn.addEventListener("click", hidePopup);

// Evento click para el botón "encode"
function encode_f(){
  const text = editor.value;
  const encodedText = btoa(text); // Codificar en Base64
  editor.value = encodedText;
}

// Evento click para el botón "decode"
function decode_f(){
  const encodedText = editor.value;
  const decodedText = atob(encodedText); // Decodificar desde Base64
  editor.value = decodedText;
}

// Evento click para el botón "share"
function share_f(){
  const text = editor.value;
  const encodedText = encodeURIComponent(text); // Codificar el texto para URL
  const currentURL = window.location.href.split('?')[0]; // Obtener la URL actual sin parámetros
  const sharedURL = `${currentURL}?text=${encodedText}`; // Construir la URL compartida
  showPopup(sharedURL);
}

// Evento click para el botón de copia (dentro del contenedor principal)
function copyMain_f(){
  const text = editor.value;
  copyToClipboard(text);
}

// Evento click para el botón de borrar
function delete_f(){
  editor.value = '';
}

// Evento click para el botón de copia (dentro del popup)
function copyPopup_f(){
  const text = editor.value;
  const encodedText = encodeURIComponent(text); // Codificar el texto para URL
  const sharedURL = `${window.location.href}?text=${encodedText}`; // Construir la URL compartida
  copyToClipboard(sharedURL);
}

// Mostrar el popup con la URL
function showPopup(url) {
  const popupURL = document.getElementById('popupURL');
  popupURL.textContent = url;
  popup.classList.add('active');
}

// Ocultar el popup
function hidePopup() {
  popup.classList.remove('active');
}

// Función para copiar al portapapeles
function copyToClipboard(text) {
  const tempInput = document.createElement('input');
  tempInput.value = text;
  document.body.appendChild(tempInput);
  tempInput.select();
  document.execCommand('copy');
  document.body.removeChild(tempInput);

  alert("Copied to clipboard");
}

// Obtener el texto de la URL si está presente
const urlParams = new URLSearchParams(window.location.search);
const encodedText = urlParams.get('text');
if (encodedText) {
  const decodedText = decodeURIComponent(encodedText); // Decodificar el texto de la URL
  editor.value = decodedText;
}